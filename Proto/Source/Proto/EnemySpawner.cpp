// Fill out your copyright notice in the Description page of Project Settings.

#include "Proto.h"
#include "EnemySpawner.h"
#include "Engine.h"

AEnemySpawner::AEnemySpawner(){
	SpawnParameters.bNoCollisionFail = true;
}

void AEnemySpawner::BeginPlay(){
	OnActorBeginOverlap.AddDynamic(this, &AEnemySpawner::OnOverlapBegin);

	if (UseActorLocation){
		Location = SpawnActorLocation->GetActorLocation() + FVector(0.0f, 0.0f, 120.0f);
	}

}

void AEnemySpawner::OnOverlapBegin(AActor* OtherActor){
	if (OtherActor->ActorHasTag(TEXT("Player"))){
		if (EnemyClass != nullptr){
			GetWorldTimerManager().SetTimer(Handle, this, &AEnemySpawner::SpawnEnemy, SpawnDelay, true);
		}
	}
}

void AEnemySpawner::SpawnEnemy(){
	GetWorld()->SpawnActor<AActor>(EnemyClass, Location, FRotator::ZeroRotator, SpawnParameters);
	SpawnCount++;
	
	if (SpawnCount >= SpawnAmount){
		GetWorldTimerManager().ClearTimer(Handle);
		SpawnCount = 0;
	}
}

