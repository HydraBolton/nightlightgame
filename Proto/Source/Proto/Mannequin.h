// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Mannequin.generated.h"


UCLASS()
class PROTO_API AMannequin : public ACharacter
{
	GENERATED_UCLASS_BODY()

	//Speed of the monster
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float slowspeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float maxspeed;

	//EnemyHealth
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float health;

	//Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float attackDamage;

	//Time between attacks
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float attackTime;

	//Last attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		float TimeSinceLastAttack;

	//Sight Range
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		USphereComponent* sightRange;

	//Slow Range
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		USphereComponent* slowRange;

	//Attack Range
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MannequinProperties)
		USphereComponent* attackRange;


public:
	
	// Sets default values for this character's properties
	AMannequin();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;


	
	
};
