// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/TriggerBox.h"
#include "EnemySpawner.generated.h"

/**
 * 
 */
UCLASS()
class PROTO_API AEnemySpawner : public ATriggerBox
{
	GENERATED_BODY()
	


public:
	AEnemySpawner();

	virtual void BeginPlay() override;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere)
	UClass* EnemyClass;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere)
	FVector Location;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere)
	bool UseActorLocation;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere)
	AActor* SpawnActorLocation;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere, meta = (UIMin = "0.0", UIMax = "100.0"))
	int32 SpawnAmount;

	UPROPERTY(Category = Spawner, BlueprintReadWrite, EditAnywhere)
	float SpawnDelay;

	FActorSpawnParameters SpawnParameters;

	UFUNCTION()
	void OnOverlapBegin(AActor* OtherActor);

	UFUNCTION()
	void SpawnEnemy();

		// Called when the game starts or when spawned
	
private:

	FTimerHandle Handle;

	int SpawnCount = 0;
	
};
