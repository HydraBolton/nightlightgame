// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MannequinAttack.generated.h"

class AMannequin;

UCLASS()
class PROTO_API AMannequinAttack : public AActor
{
	GENERATED_UCLASS_BODY()

	//Damage of attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Attack)
	float damage;

	//Array of actors hit by current attack
	TArray<AActor*>ActorsHit;

	bool attacking;

	AMannequin* myself;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Attack)
		UBoxComponent* ProxyBox;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Attack)
		UStaticMeshComponent* mesh;

	UFUNCTION(BlueprintNativeEvent, category = Collision)
		void Prox(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
	void Swing();
	void Rest();
	
public:	
	// Sets default values for this actor's properties
	AMannequinAttack();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
