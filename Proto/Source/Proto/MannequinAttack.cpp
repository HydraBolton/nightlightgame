// Fill out your copyright notice in the Description page of Project Settings.

#include "Proto.h"
#include "MannequinAttack.h"


// Sets default values
AMannequinAttack::AMannequinAttack()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}
AMannequinAttack::AMannequinAttack(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){
	damage = 1;
	attacking = false;
	myself = NULL;

	mesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("mesh"));
	RootComponent = mesh;

	ProxyBox = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("ProxyBox"));
	ProxyBox->OnComponentBeginOverlap.AddDynamic(this, &AMannequinAttack::Prox);
	ProxyBox->AttachTo(RootComponent);

}

void AMannequinAttack::Prox_Implementation(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult){
	if (OtherComp != OtherActor->GetRootComponent()){

		return;
	}
	/*
	if (attacking && OtherActor != myself && !ActorsHit.Contains(OtherActor)){
		OtherActor->TakeDamage(damage + myself->attackDamage, FDamageEvent(), NULL, this);
		ActorsHit.Add(OtherActor);

	}*/
}

void AMannequinAttack::Swing(){

	ActorsHit.Empty();
	attacking = true;
}

void AMannequinAttack::Rest(){
	ActorsHit.Empty();
	attacking = false;
}

// Called when the game starts or when spawned
void AMannequinAttack::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMannequinAttack::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

