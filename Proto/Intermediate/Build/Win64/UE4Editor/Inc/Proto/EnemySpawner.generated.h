// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef PROTO_EnemySpawner_generated_h
#error "EnemySpawner.generated.h already included, missing '#pragma once' in EnemySpawner.h"
#endif
#define PROTO_EnemySpawner_generated_h

#define Proto_Source_Proto_EnemySpawner_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnEnemy) \
	{ \
		P_FINISH; \
		this->SpawnEnemy(); \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		this->OnOverlapBegin(Z_Param_OtherActor); \
	}


#define Proto_Source_Proto_EnemySpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnEnemy) \
	{ \
		P_FINISH; \
		this->SpawnEnemy(); \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		this->OnOverlapBegin(Z_Param_OtherActor); \
	}


#define Proto_Source_Proto_EnemySpawner_h_14_INCLASS_NO_PURE_DECLS \
	private: \
	static void StaticRegisterNativesAEnemySpawner(); \
	friend PROTO_API class UClass* Z_Construct_UClass_AEnemySpawner(); \
	public: \
	DECLARE_CLASS(AEnemySpawner, ATriggerBox, COMPILED_IN_FLAGS(0), 0, Proto, NO_API) \
	DECLARE_SERIALIZER(AEnemySpawner) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AEnemySpawner*>(this); }


#define Proto_Source_Proto_EnemySpawner_h_14_INCLASS \
	private: \
	static void StaticRegisterNativesAEnemySpawner(); \
	friend PROTO_API class UClass* Z_Construct_UClass_AEnemySpawner(); \
	public: \
	DECLARE_CLASS(AEnemySpawner, ATriggerBox, COMPILED_IN_FLAGS(0), 0, Proto, NO_API) \
	DECLARE_SERIALIZER(AEnemySpawner) \
	/** Indicates whether the class is compiled into the engine */    enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AEnemySpawner*>(this); }


#define Proto_Source_Proto_EnemySpawner_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemySpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemySpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemySpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemySpawner); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AEnemySpawner(const AEnemySpawner& InCopy); \
public:


#define Proto_Source_Proto_EnemySpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API AEnemySpawner(const AEnemySpawner& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemySpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemySpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemySpawner)


#define Proto_Source_Proto_EnemySpawner_h_11_PROLOG
#define Proto_Source_Proto_EnemySpawner_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Proto_Source_Proto_EnemySpawner_h_14_RPC_WRAPPERS \
	Proto_Source_Proto_EnemySpawner_h_14_INCLASS \
	Proto_Source_Proto_EnemySpawner_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Proto_Source_Proto_EnemySpawner_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Proto_Source_Proto_EnemySpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Proto_Source_Proto_EnemySpawner_h_14_INCLASS_NO_PURE_DECLS \
	Proto_Source_Proto_EnemySpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Proto_Source_Proto_EnemySpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
