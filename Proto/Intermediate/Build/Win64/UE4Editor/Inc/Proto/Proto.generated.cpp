// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "Proto.h"
#include "Proto.generated.dep.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeProto() {}
	void AEnemySpawner::StaticRegisterNativesAEnemySpawner()
	{
		FNativeFunctionRegistrar::RegisterFunction(AEnemySpawner::StaticClass(),"OnOverlapBegin",(Native)&AEnemySpawner::execOnOverlapBegin);
		FNativeFunctionRegistrar::RegisterFunction(AEnemySpawner::StaticClass(),"SpawnEnemy",(Native)&AEnemySpawner::execSpawnEnemy);
	}
	IMPLEMENT_CLASS(AEnemySpawner, 1309704534);
	void AGameModeNightLight::StaticRegisterNativesAGameModeNightLight()
	{
	}
	IMPLEMENT_CLASS(AGameModeNightLight, 1219282702);
	void AMannequin::StaticRegisterNativesAMannequin()
	{
	}
	IMPLEMENT_CLASS(AMannequin, 3048054676);
	void AMannequinAttack::Prox(AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
	{
		MannequinAttack_eventProx_Parms Parms;
		Parms.OtherActor=OtherActor;
		Parms.OtherComp=OtherComp;
		Parms.OtherBodyIndex=OtherBodyIndex;
		Parms.bFromSweep=bFromSweep ? true : false;
		Parms.SweepResult=SweepResult;
		ProcessEvent(FindFunctionChecked(PROTO_Prox),&Parms);
	}
	void AMannequinAttack::StaticRegisterNativesAMannequinAttack()
	{
		FNativeFunctionRegistrar::RegisterFunction(AMannequinAttack::StaticClass(),"Prox",(Native)&AMannequinAttack::execProx);
	}
	IMPLEMENT_CLASS(AMannequinAttack, 569390005);
	void APlayerCharacter::StaticRegisterNativesAPlayerCharacter()
	{
	}
	IMPLEMENT_CLASS(APlayerCharacter, 3120214664);
	void AProtoCharacter::StaticRegisterNativesAProtoCharacter()
	{
	}
	IMPLEMENT_CLASS(AProtoCharacter, 322374401);
	void AProtoGameMode::StaticRegisterNativesAProtoGameMode()
	{
	}
	IMPLEMENT_CLASS(AProtoGameMode, 520479844);
FName PROTO_Prox = FName(TEXT("Prox"));
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_ATriggerBox();
	COREUOBJECT_API class UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API class UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AGameMode();
	ENGINE_API class UClass* Z_Construct_UClass_ACharacter();
	ENGINE_API class UClass* Z_Construct_UClass_USphereComponent_NoRegister();
	ENGINE_API class UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API class UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();
	ENGINE_API class UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();

	PROTO_API class UFunction* Z_Construct_UFunction_AEnemySpawner_OnOverlapBegin();
	PROTO_API class UFunction* Z_Construct_UFunction_AEnemySpawner_SpawnEnemy();
	PROTO_API class UClass* Z_Construct_UClass_AEnemySpawner_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AEnemySpawner();
	PROTO_API class UClass* Z_Construct_UClass_AGameModeNightLight_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AGameModeNightLight();
	PROTO_API class UClass* Z_Construct_UClass_AMannequin_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AMannequin();
	PROTO_API class UFunction* Z_Construct_UFunction_AMannequinAttack_Prox();
	PROTO_API class UClass* Z_Construct_UClass_AMannequinAttack_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AMannequinAttack();
	PROTO_API class UClass* Z_Construct_UClass_APlayerCharacter_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_APlayerCharacter();
	PROTO_API class UClass* Z_Construct_UClass_AProtoCharacter_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AProtoCharacter();
	PROTO_API class UClass* Z_Construct_UClass_AProtoGameMode_NoRegister();
	PROTO_API class UClass* Z_Construct_UClass_AProtoGameMode();
	PROTO_API class UPackage* Z_Construct_UPackage_Proto();
	UFunction* Z_Construct_UFunction_AEnemySpawner_OnOverlapBegin()
	{
		struct EnemySpawner_eventOnOverlapBegin_Parms
		{
			AActor* OtherActor;
		};
		UObject* Outer=Z_Construct_UClass_AEnemySpawner();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("OnOverlapBegin"), RF_Public|RF_Transient|RF_Native) UFunction(FObjectInitializer(), NULL, 0x00020401, 65535, sizeof(EnemySpawner_eventOnOverlapBegin_Parms));
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, EnemySpawner_eventOnOverlapBegin_Parms), 0x0000000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AEnemySpawner_SpawnEnemy()
	{
		UObject* Outer=Z_Construct_UClass_AEnemySpawner();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SpawnEnemy"), RF_Public|RF_Transient|RF_Native) UFunction(FObjectInitializer(), NULL, 0x00020401, 65535);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AEnemySpawner_NoRegister()
	{
		return AEnemySpawner::StaticClass();
	}
	UClass* Z_Construct_UClass_AEnemySpawner()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ATriggerBox();
			Z_Construct_UPackage_Proto();
			OuterClass = AEnemySpawner::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_AEnemySpawner_OnOverlapBegin());
				OuterClass->LinkChild(Z_Construct_UFunction_AEnemySpawner_SpawnEnemy());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_SpawnDelay = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpawnDelay"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(SpawnDelay, AEnemySpawner), 0x0000000000000005);
				UProperty* NewProp_SpawnAmount = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpawnAmount"), RF_Public|RF_Transient|RF_Native) UIntProperty(CPP_PROPERTY_BASE(SpawnAmount, AEnemySpawner), 0x0000000000000005);
				UProperty* NewProp_SpawnActorLocation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SpawnActorLocation"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(SpawnActorLocation, AEnemySpawner), 0x0000000000000005, Z_Construct_UClass_AActor_NoRegister());
				CPP_BOOL_PROPERTY_BITMASK_STRUCT(UseActorLocation, AEnemySpawner, bool);
				UProperty* NewProp_UseActorLocation = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("UseActorLocation"), RF_Public|RF_Transient|RF_Native) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(UseActorLocation, AEnemySpawner), 0x0000000000000005, CPP_BOOL_PROPERTY_BITMASK(UseActorLocation, AEnemySpawner), sizeof(bool), true);
				UProperty* NewProp_Location = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("Location"), RF_Public|RF_Transient|RF_Native) UStructProperty(CPP_PROPERTY_BASE(Location, AEnemySpawner), 0x0000000000000005, Z_Construct_UScriptStruct_FVector());
				UProperty* NewProp_EnemyClass = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EnemyClass"), RF_Public|RF_Transient|RF_Native) UClassProperty(CPP_PROPERTY_BASE(EnemyClass, AEnemySpawner), 0x0000000000000005, Z_Construct_UClass_UObject_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMap(Z_Construct_UFunction_AEnemySpawner_OnOverlapBegin()); // 2340247376
				OuterClass->AddFunctionToFunctionMap(Z_Construct_UFunction_AEnemySpawner_SpawnEnemy()); // 2247877130
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_SpawnDelay, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_SpawnDelay, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_SpawnAmount, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_SpawnAmount, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_SpawnAmount, TEXT("UIMax"), TEXT("100.0"));
				MetaData->SetValue(NewProp_SpawnAmount, TEXT("UIMin"), TEXT("0.0"));
				MetaData->SetValue(NewProp_SpawnActorLocation, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_SpawnActorLocation, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_UseActorLocation, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_UseActorLocation, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_Location, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_Location, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
				MetaData->SetValue(NewProp_EnemyClass, TEXT("Category"), TEXT("Spawner"));
				MetaData->SetValue(NewProp_EnemyClass, TEXT("ModuleRelativePath"), TEXT("EnemySpawner.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEnemySpawner(Z_Construct_UClass_AEnemySpawner, TEXT("AEnemySpawner"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEnemySpawner);
	UClass* Z_Construct_UClass_AGameModeNightLight_NoRegister()
	{
		return AGameModeNightLight::StaticClass();
	}
	UClass* Z_Construct_UClass_AGameModeNightLight()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameMode();
			Z_Construct_UPackage_Proto();
			OuterClass = AGameModeNightLight::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2090028C;


				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("GameModeNightLight.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("GameModeNightLight.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGameModeNightLight(Z_Construct_UClass_AGameModeNightLight, TEXT("AGameModeNightLight"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGameModeNightLight);
	UClass* Z_Construct_UClass_AMannequin_NoRegister()
	{
		return AMannequin::StaticClass();
	}
	UClass* Z_Construct_UClass_AMannequin()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage_Proto();
			OuterClass = AMannequin::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_attackRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("attackRange"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(attackRange, AMannequin), 0x000000000008000d, Z_Construct_UClass_USphereComponent_NoRegister());
				UProperty* NewProp_slowRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("slowRange"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(slowRange, AMannequin), 0x000000000008000d, Z_Construct_UClass_USphereComponent_NoRegister());
				UProperty* NewProp_sightRange = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("sightRange"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(sightRange, AMannequin), 0x000000000008000d, Z_Construct_UClass_USphereComponent_NoRegister());
				UProperty* NewProp_TimeSinceLastAttack = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("TimeSinceLastAttack"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(TimeSinceLastAttack, AMannequin), 0x0000000000000005);
				UProperty* NewProp_attackTime = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("attackTime"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(attackTime, AMannequin), 0x0000000000000005);
				UProperty* NewProp_attackDamage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("attackDamage"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(attackDamage, AMannequin), 0x0000000000000005);
				UProperty* NewProp_health = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("health"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(health, AMannequin), 0x0000000000000005);
				UProperty* NewProp_maxspeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("maxspeed"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(maxspeed, AMannequin), 0x0000000000000005);
				UProperty* NewProp_slowspeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("slowspeed"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(slowspeed, AMannequin), 0x0000000000000005);
				UProperty* NewProp_speed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("speed"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(speed, AMannequin), 0x0000000000000005);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_attackRange, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_attackRange, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_attackRange, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_attackRange, TEXT("ToolTip"), TEXT("Attack Range"));
				MetaData->SetValue(NewProp_slowRange, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_slowRange, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_slowRange, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_slowRange, TEXT("ToolTip"), TEXT("Slow Range"));
				MetaData->SetValue(NewProp_sightRange, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_sightRange, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_sightRange, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_sightRange, TEXT("ToolTip"), TEXT("Sight Range"));
				MetaData->SetValue(NewProp_TimeSinceLastAttack, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_TimeSinceLastAttack, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_TimeSinceLastAttack, TEXT("ToolTip"), TEXT("Last attack"));
				MetaData->SetValue(NewProp_attackTime, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_attackTime, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_attackTime, TEXT("ToolTip"), TEXT("Time between attacks"));
				MetaData->SetValue(NewProp_attackDamage, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_attackDamage, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_attackDamage, TEXT("ToolTip"), TEXT("Damage"));
				MetaData->SetValue(NewProp_health, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_health, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_health, TEXT("ToolTip"), TEXT("EnemyHealth"));
				MetaData->SetValue(NewProp_maxspeed, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_maxspeed, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_slowspeed, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_slowspeed, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_speed, TEXT("Category"), TEXT("MannequinProperties"));
				MetaData->SetValue(NewProp_speed, TEXT("ModuleRelativePath"), TEXT("Mannequin.h"));
				MetaData->SetValue(NewProp_speed, TEXT("ToolTip"), TEXT("Speed of the monster"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMannequin(Z_Construct_UClass_AMannequin, TEXT("AMannequin"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMannequin);
	UFunction* Z_Construct_UFunction_AMannequinAttack_Prox()
	{
		UObject* Outer=Z_Construct_UClass_AMannequinAttack();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("Prox"), RF_Public|RF_Transient|RF_Native) UFunction(FObjectInitializer(), NULL, 0x08420C00, 65535, sizeof(MannequinAttack_eventProx_Parms));
			UProperty* NewProp_SweepResult = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("SweepResult"), RF_Public|RF_Transient|RF_Native) UStructProperty(CPP_PROPERTY_BASE(SweepResult, MannequinAttack_eventProx_Parms), 0x0000008008000182, Z_Construct_UScriptStruct_FHitResult());
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(bFromSweep, MannequinAttack_eventProx_Parms, bool);
			UProperty* NewProp_bFromSweep = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("bFromSweep"), RF_Public|RF_Transient|RF_Native) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(bFromSweep, MannequinAttack_eventProx_Parms), 0x0000000000000080, CPP_BOOL_PROPERTY_BITMASK(bFromSweep, MannequinAttack_eventProx_Parms), sizeof(bool), true);
			UProperty* NewProp_OtherBodyIndex = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherBodyIndex"), RF_Public|RF_Transient|RF_Native) UIntProperty(CPP_PROPERTY_BASE(OtherBodyIndex, MannequinAttack_eventProx_Parms), 0x0000000000000080);
			UProperty* NewProp_OtherComp = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherComp"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(OtherComp, MannequinAttack_eventProx_Parms), 0x0000000000080080, Z_Construct_UClass_UPrimitiveComponent_NoRegister());
			UProperty* NewProp_OtherActor = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("OtherActor"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(OtherActor, MannequinAttack_eventProx_Parms), 0x0000000000000080, Z_Construct_UClass_AActor_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Collision"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("MannequinAttack.h"));
			MetaData->SetValue(NewProp_OtherComp, TEXT("EditInline"), TEXT("true"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMannequinAttack_NoRegister()
	{
		return AMannequinAttack::StaticClass();
	}
	UClass* Z_Construct_UClass_AMannequinAttack()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage_Proto();
			OuterClass = AMannequinAttack::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_AMannequinAttack_Prox());

PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_mesh = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("mesh"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(mesh, AMannequinAttack), 0x00000000000b001d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				UProperty* NewProp_ProxyBox = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ProxyBox"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(ProxyBox, AMannequinAttack), 0x00000000000b001d, Z_Construct_UClass_UBoxComponent_NoRegister());
				UProperty* NewProp_damage = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("damage"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(damage, AMannequinAttack), 0x0000000000000005);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->AddFunctionToFunctionMap(Z_Construct_UFunction_AMannequinAttack_Prox()); // 137719685
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("MannequinAttack.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("MannequinAttack.h"));
				MetaData->SetValue(NewProp_mesh, TEXT("Category"), TEXT("Attack"));
				MetaData->SetValue(NewProp_mesh, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_mesh, TEXT("ModuleRelativePath"), TEXT("MannequinAttack.h"));
				MetaData->SetValue(NewProp_ProxyBox, TEXT("Category"), TEXT("Attack"));
				MetaData->SetValue(NewProp_ProxyBox, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_ProxyBox, TEXT("ModuleRelativePath"), TEXT("MannequinAttack.h"));
				MetaData->SetValue(NewProp_damage, TEXT("Category"), TEXT("Attack"));
				MetaData->SetValue(NewProp_damage, TEXT("ModuleRelativePath"), TEXT("MannequinAttack.h"));
				MetaData->SetValue(NewProp_damage, TEXT("ToolTip"), TEXT("Damage of attack"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMannequinAttack(Z_Construct_UClass_AMannequinAttack, TEXT("AMannequinAttack"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMannequinAttack);
	UClass* Z_Construct_UClass_APlayerCharacter_NoRegister()
	{
		return APlayerCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_APlayerCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage_Proto();
			OuterClass = APlayerCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;


				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerCharacter(Z_Construct_UClass_APlayerCharacter, TEXT("APlayerCharacter"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerCharacter);
	UClass* Z_Construct_UClass_AProtoCharacter_NoRegister()
	{
		return AProtoCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_AProtoCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage_Proto();
			OuterClass = AProtoCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20800080;


PRAGMA_DISABLE_DEPRECATION_WARNINGS
				UProperty* NewProp_BaseLookUpRate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BaseLookUpRate"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(BaseLookUpRate, AProtoCharacter), 0x0000000000020015);
				UProperty* NewProp_BaseTurnRate = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("BaseTurnRate"), RF_Public|RF_Transient|RF_Native) UFloatProperty(CPP_PROPERTY_BASE(BaseTurnRate, AProtoCharacter), 0x0000000000020015);
				UProperty* NewProp_FollowCamera = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("FollowCamera"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(FollowCamera, AProtoCharacter), 0x00000000000a001d, Z_Construct_UClass_UCameraComponent_NoRegister());
				UProperty* NewProp_CameraBoom = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraBoom"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CameraBoom, AProtoCharacter), 0x00000000000a001d, Z_Construct_UClass_USpringArmComponent_NoRegister());
PRAGMA_ENABLE_DEPRECATION_WARNINGS
				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(NewProp_BaseLookUpRate, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_BaseLookUpRate, TEXT("ModuleRelativePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(NewProp_BaseLookUpRate, TEXT("ToolTip"), TEXT("Base look up/down rate, in deg/sec. Other scaling may affect final rate."));
				MetaData->SetValue(NewProp_BaseTurnRate, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_BaseTurnRate, TEXT("ModuleRelativePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(NewProp_BaseTurnRate, TEXT("ToolTip"), TEXT("Base turn rate, in deg/sec. Other scaling may affect final turn rate."));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("ModuleRelativePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(NewProp_FollowCamera, TEXT("ToolTip"), TEXT("Follow camera"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("Category"), TEXT("Camera"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("ModuleRelativePath"), TEXT("ProtoCharacter.h"));
				MetaData->SetValue(NewProp_CameraBoom, TEXT("ToolTip"), TEXT("Camera boom positioning the camera behind the character"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProtoCharacter(Z_Construct_UClass_AProtoCharacter, TEXT("AProtoCharacter"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProtoCharacter);
	UClass* Z_Construct_UClass_AProtoGameMode_NoRegister()
	{
		return AProtoGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_AProtoGameMode()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AGameMode();
			Z_Construct_UPackage_Proto();
			OuterClass = AProtoGameMode::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x2088028C;


				OuterClass->ClassConfigName = FName(TEXT("Game"));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("ProtoGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("ProtoGameMode.h"));
				MetaData->SetValue(OuterClass, TEXT("ShowCategories"), TEXT("Input|MouseInput Input|TouchInput"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AProtoGameMode(Z_Construct_UClass_AProtoGameMode, TEXT("AProtoGameMode"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(AProtoGameMode);
	UPackage* Z_Construct_UPackage_Proto()
	{
		static UPackage* ReturnPackage = NULL;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), NULL, FName(TEXT("/Script/Proto")), false, false));
			ReturnPackage->PackageFlags |= PKG_CompiledIn | 0x00000000;
			FGuid Guid;
			Guid.A = 0x9F081D6A;
			Guid.B = 0xA2B6679F;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif

PRAGMA_ENABLE_DEPRECATION_WARNINGS
